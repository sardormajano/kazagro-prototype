




function StaticMessageManager()
{
    var staticJSMessages = [];

    var monthList = {
        'January':   {name:      'Январь',
            shortName: 'Янв'},
        'February':  {name:      'Февраль',
            shortName: 'Фев'},
        'March':     {name:      'Март',
            shortName: 'Мар'},
        'April':     {name:      'Апрель',
            shortName: 'Апр'},
        'May':       {name:      'Май',
            shortName: 'Май'},
        'June':      {name:      'Июнь',
            shortName: 'Июн'},
        'July':      {name:      'Июль',
            shortName: 'Июл'},
        'August':    {name:      'Август',
            shortName: 'Авг'},
        'September': {name:      'Сентябрь',
            shortName: 'Сен'},
        'October':   {name:      'Октябрь',
            shortName: 'Окт'},
        'November':  {name:      'Ноябрь',
            shortName: 'Ноя'},
        'December':  {name:      'Декабрь',
            shortName: 'Дек'}
    };

    var dayList =   {
        'Monday'    : {name:      'Понедельник',
            shortName: 'Пн'},
        'Tuesday'   : {name:      'Вторник',
            shortName: 'Вт'},
        'Wednesday' : {name:      'Среда',
            shortName: 'Ср'},
        'Thursday'  : {name:      'Четверг',
            shortName: 'Чт'},
        'Friday'    : {name:      'Пятница',
            shortName: 'Пт'},
        'Saturday'  : {name:      'Суббота',
            shortName: 'Сб'},
        'Sunday'    : {name:      'Воскресенье',
            shortName: 'Вс'}
    };

    function registerStaticJSMessage(bundle, key, message)
    {
        staticJSMessages[bundle + '|' + key] = message;
    }

    registerStaticJSMessage('commonBundle', 'datePicker.TEXT_PREV_YEAR',   'Предыдущий год');
    registerStaticJSMessage('commonBundle', 'datePicker.TEXT_PREV_MONTH',  'Предыдущий месяц');
    registerStaticJSMessage('commonBundle', 'datePicker.TEXT_NEXT_YEAR',   'Следующий год');
    registerStaticJSMessage('commonBundle', 'datePicker.TEXT_NEXT_MONTH',  'Следующий месяц');
    registerStaticJSMessage('commonBundle', 'datePicker.TEXT_CLOSE',       'отмена');
    registerStaticJSMessage('commonBundle', 'datePicker.TEXT_CHOOSE_DATE', 'Выбрать дату');

    registerStaticJSMessage('recoverBundle', 'label.required', 'Поле обязательно для&nbsp;заполнения');
    registerStaticJSMessage('recoverBundle', 'label.minLen', 'Минимум {n} символов');
    registerStaticJSMessage('recoverBundle', 'label.maxLen', 'Максимум {n} символов');
    registerStaticJSMessage('recoverBundle', 'label.email', 'Неправильный адрес электронной почты');
    registerStaticJSMessage('recoverBundle', 'label.captcha', 'Введенный код не&nbsp;совпадает с&nbsp;кодом на&nbsp;картинке');
    registerStaticJSMessage('recoverBundle', 'label.card', 'Вы неправильно указали номер карты. Пожалуйста, проверьте количество цифр введенного номера карты и&nbsp;их последовательность.');
    registerStaticJSMessage('recoverBundle', 'label.sms_required', 'Введите цифровой пятизначный SMS-пароль');
    registerStaticJSMessage('recoverBundle', 'label.card_required', 'Необходимо ввести номер вашей карты Сбербанка');
    registerStaticJSMessage('recoverBundle', 'label.login', 'Недопустимые символы. Проверьте раскладку клавиатуры');
    registerStaticJSMessage('recoverBundle', 'label.login_letter', 'Логин должен содержать хотя бы одну букву');
    registerStaticJSMessage('recoverBundle', 'label.login_number', 'Логин должен содержать хотя бы одну цифру');
    registerStaticJSMessage('recoverBundle', 'label.login_letter_or_digit', 'Логин должен содержать хотя бы одну букву или цифру');
    registerStaticJSMessage('recoverBundle', 'label.login_ten_digit', 'Логин не может состоять из 10 цифр');
    registerStaticJSMessage('recoverBundle', 'label.login_let3', 'Логин не должен содержать более 3-х одинаковых символов подряд');
    registerStaticJSMessage('recoverBundle', 'label.login_key3', 'Логин не должен содержать более 3-х символов, расположенных в&nbsp;одном ряду клавиатуры');
    registerStaticJSMessage('recoverBundle', 'label.login_maxLen', 'Слишком длинный логин. Максимально {n} символов');
    registerStaticJSMessage('recoverBundle', 'label.login_minLen', 'Слишком короткий логин. Минимальная длина {n} символов');
    registerStaticJSMessage('recoverBundle', 'label.login_avail', 'Указанный логин занят. Пользователь с&nbsp;таким логином уже зарегистрирован в&nbsp;системе. Попробуйте использовать другой.');
    registerStaticJSMessage('recoverBundle', 'label.password', 'Недопустимые символы. Проверьте раскладку клавиатуры');
    registerStaticJSMessage('recoverBundle', 'label.password_letter', 'Пароль должен содержать хотя бы одну букву');
    registerStaticJSMessage('recoverBundle', 'label.password_number', 'Пароль должен содержать хотя бы одну цифру');
    registerStaticJSMessage('recoverBundle', 'label.password_let3', 'Пароль не должен содержать более 3-х одинаковых символов подряд');
    registerStaticJSMessage('recoverBundle', 'label.password_key3', 'Пароль не должен содержать более 3-х символов, расположенных в&nbsp;одном ряду клавиатуры');
    registerStaticJSMessage('recoverBundle', 'label.password_maxLen', 'Слишком длинный пароль. Максимально {n} символов');
    registerStaticJSMessage('recoverBundle', 'label.password_minLen', 'Слишком короткий пароль. Минимальная длина {n} символов');
    registerStaticJSMessage('recoverBundle', 'label.password_differ', 'Пароль не должен совпадать с&nbsp;логином');
    registerStaticJSMessage('recoverBundle', 'label.password_compare', 'Введенные пароли не&nbsp;совпадают');
    registerStaticJSMessage('recoverBundle', 'label.password_avail', 'Введенный пароль совпадает с&nbsp;предыдущим. Пожалуйста, укажите другой пароль');
    registerStaticJSMessage('recoverBundle', 'label.timer_get_sms', 'Срок действия пароля истек, получите новый пароль, нажав на ссылку &quot;Выслать новый SMS-пароль&quot;');
    registerStaticJSMessage('recoverBundle', 'label.timer_get_reg', 'Срок действия пароля истек, пройдите регистрацию заново');
    registerStaticJSMessage('recoverBundle', 'label.timer_get_recover', 'Срок действия пароля истёк, начните процесс восстановления пароля заново');
    registerStaticJSMessage('commonBundle', 'validator.message.required', 'Заполните поле');
    registerStaticJSMessage('commonBundle', 'validator.message.minLen', 'Введите минимум {n} символа');
    registerStaticJSMessage('commonBundle', 'validator.message.maxLen', 'Введите максимум {n} символов');
    registerStaticJSMessage('commonBundle', 'validator.message.required.login', 'Введите идентификатор или&nbsp;логин пользователя');
    registerStaticJSMessage('commonBundle', 'validator.message.required.password', 'Введите пароль');
    registerStaticJSMessage('commonBundle', 'validator.message.required.captcha', 'Введите код с картинки');

    this.getStaticJSMessage = function(bundle, key)
    {
        return staticJSMessages[bundle + '|' + key];
    };

    this.getMonthInfo = function(monthKey)
    {
        return monthList[monthKey];
    };

    this.getDayInfo = function(dayKey)
    {
        return dayList[dayKey];
    }
}

var STATIC_MESSAGE_MANAGER = new StaticMessageManager();

function getStaticJSMessage(bundle, key)
{
    return STATIC_MESSAGE_MANAGER.getStaticJSMessage(bundle, key);
}

function getMonthInfo(monthKey)
{
    return STATIC_MESSAGE_MANAGER.getMonthInfo(monthKey);
}

function getDayInfo(dayKey)
{
    return STATIC_MESSAGE_MANAGER.getDayInfo(dayKey);
}

